FROM python:3

WORKDIR /app
ADD requirements.txt /app/
RUN pip install -r /app/requirements.txt

ADD . .

CMD python3 /app/app.py


